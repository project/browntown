<?php
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
<!-- Drupal themes designed by DrupalThemeBank.com.
 Created Feb 20, 2009, Last Updated: Feb 25, 2009
-->

 <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>

<link rel="stylesheet" type="text/css" href="style.css" />

    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="<?php print base_path() . path_to_theme() ?>/fix-ie.css" />
      <![endif]-->
</head>
<body>
<div id="conteneur">
  <div id="header">
  <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
      <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
      <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
    </div>
  <div id="menu">&nbsp;</div>
  <div id="sousmenu">&nbsp;</div>
  <div id="center">
    <div id="gauche">
      <div id="nav">   <?php print $sidebar_left ?></div><br />
      </div>
    <div id="contenu">
	 <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>

       <?php print $breadcrumb ?>
        <h1 class="title"><?php print $title ?></h1>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?>
        </div>
  </div>

  <div id="pied"><p id="copy">
  <?php print $footer_message ?><br />
  <?php
include(path_to_theme().'/footer.php');
?>      </p>
</div>
 </div>


<?php print $closure ?>
</body>
</html>
